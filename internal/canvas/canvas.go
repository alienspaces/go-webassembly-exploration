package canvas

import (
	"image"
	"syscall/js"

	"github.com/golang/freetype/truetype"
	"github.com/llgcode/draw2d"
	"github.com/llgcode/draw2d/draw2dimg"
)

// RenderFunc -
type RenderFunc func(c *Canvas) bool

// Canvas -
type Canvas struct {

	// Used as part of 'run forever' in the render handler
	done chan struct{}

	// DOM properties
	window     js.Value
	doc        js.Value
	body       js.Value
	windowSize struct{ w, h float64 }

	// Canvas properties
	canvas js.Value
	ctx    js.Value
	im     js.Value
	width  int
	height int

	// Drawing Context
	gctx     *draw2dimg.GraphicContext // Graphic Context
	image    *image.RGBA               // The Shadow frame we actually draw on
	font     *truetype.Font
	fontData draw2d.FontData

	reqID    js.Value // Storage of the current animationFrame requestID - For Cancel
	timeStep float64  // Min Time delay between frames. - Calculated as   maxFPS/1000
}

// NewCanvas -
func NewCanvas(create bool) (*Canvas, error) {

	var c Canvas

	c.window = js.Global()
	c.doc = c.window.Get("document")
	c.body = c.doc.Get("body")

	c.windowSize.h = c.window.Get("innerHeight").Float()
	c.windowSize.w = c.window.Get("innerWidth").Float()

	// If create, make a canvas that fills the windows
	if create {
		c.Create(int(c.windowSize.w), int(c.windowSize.h))
	}

	return &c, nil
}

// Create a new Canvas in the DOM, and append it to the Body.
// This also calls Set to create relevant shadow Buffer etc

// Create -
func (c *Canvas) Create(width int, height int) {

	// canvas element
	canvas := c.doc.Call("createElement", "canvas")

	canvas.Set("height", height)
	canvas.Set("width", width)
	c.body.Call("appendChild", canvas)

	c.canvas = canvas
	c.height = height
	c.width = width

	// setup the 2D Drawing context
	c.ctx = c.canvas.Call("getContext", "2d")
	c.im = c.ctx.Call("createImageData", c.windowSize.w, c.windowSize.h) // Note Width, then Height

	c.image = image.NewRGBA(image.Rect(0, 0, width, height))
	c.gctx = draw2dimg.NewGraphicContext(c.image)

	// init font
	c.font, _ = truetype.Parse(FontData["font.ttf"])

	c.fontData = draw2d.FontData{
		Name:   "roboto",
		Family: draw2d.FontFamilySans,
		Style:  draw2d.FontStyleNormal,
	}
	fontCache := &FontCache{}
	fontCache.Store(c.fontData, c.font)

	c.gctx.FontCache = fontCache
}

// Start - Starts the animationFrame callbacks running
func (c *Canvas) Start(maxFPS float64, rf RenderFunc) {
	c.SetFPS(maxFPS)
	c.initFrameUpdate(rf)
}

// Stop - This needs to be called on an 'beforeUnload' trigger, to properly close out the render callback
func (c *Canvas) Stop() {
	c.window.Call("cancelAnimationFrame", c.reqID)
	c.done <- struct{}{}
	close(c.done)
}

// SetFPS - Sets the maximum FPS (Frames per Second).  This can be changed on the fly and will take affect next frame.
func (c *Canvas) SetFPS(maxFPS float64) {
	c.timeStep = 1000 / maxFPS
}

// Gc - Get the Drawing context for the Canvas
func (c *Canvas) Gc() *draw2dimg.GraphicContext {
	return c.gctx
}

// Height -
func (c *Canvas) Height() int {
	return c.height
}

// Width -
func (c *Canvas) Width() int {
	return c.width
}

// handles calls from Render, and copies the image over.
func (c *Canvas) initFrameUpdate(rf RenderFunc) {

	// hold the callbacks without blocking
	go func() {
		var renderFrame js.Func
		var lastTimestamp float64

		renderFrame = js.FuncOf(func(this js.Value, args []js.Value) interface{} {

			timestamp := args[0].Float()

			// constrain FPS
			if timestamp-lastTimestamp >= c.timeStep {

				// call the requested render function, before copying the frame
				if rf != nil {

					// only copy the image back if RenderFunction returns TRUE. (i.e. stuff has changed.)
					if rf(c) {
						c.imgCopy()
					}
				} else {
					// Just do the copy, rendering must be being done elsewhere
					c.imgCopy()
				}
				lastTimestamp = timestamp
			}

			c.reqID = js.Global().Call("requestAnimationFrame", renderFrame) // Captures the requestID to be used in Close / Cancel
			return nil
		})
		defer renderFrame.Release()
		js.Global().Call("requestAnimationFrame", renderFrame)
		<-c.done
	}()
}

// Does the actual copy over of the image data for the 'render' call.
func (c *Canvas) imgCopy() {

	dst := js.Global().Get("Uint8Array").New(len(c.image.Pix))
	_ = js.CopyBytesToJS(dst, c.image.Pix)

	c.im.Get("data").Call("set", dst)
	c.ctx.Call("putImageData", c.im, 0, 0)
}
