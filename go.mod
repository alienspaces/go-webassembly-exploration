module gitlab.com/alienspaces/go-webassembly

go 1.13

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/llgcode/draw2d v0.0.0-20190810100245-79e59b6b8fbc
)
