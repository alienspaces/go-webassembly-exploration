#!/usr/bin/env bash

go mod tidy

cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .

go build -o wasm-server ./cmd/server/main.go

GOOS=js GOARCH=wasm go build -o bundle.wasm ./cmd/wasm/main.go

./wasm-server