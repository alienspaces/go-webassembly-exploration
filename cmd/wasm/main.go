package main

import (
	"image/color"
	"time"

	"github.com/llgcode/draw2d/draw2dkit"

	"gitlab.com/alienspaces/go-webassembly/internal/canvas"
)

type gameState struct {
	laserX, laserY, directionX, directionY, laserSize float64
}

var gs = gameState{laserSize: 35, directionX: 13.7, directionY: -13.7, laserX: 40, laserY: 40}

// This specifies how long a delay between calls to 'render'.
var renderDelay time.Duration = 20 * time.Millisecond

func main() {

	var done chan struct{}

	println("FrameRate:", time.Second/renderDelay)

	c, _ := canvas.NewCanvas(true)

	c.Start(60, Render)

	<-done
}

// Render -
func Render(c *canvas.Canvas) bool {

	gc := c.Gc()
	width := float64(c.Width())
	height := float64(c.Height())

	if gs.laserX+gs.directionX > width-gs.laserSize || gs.laserX+gs.directionX < gs.laserSize {
		gs.directionX = -gs.directionX
	}
	if gs.laserY+gs.directionY > height-gs.laserSize || gs.laserY+gs.directionY < gs.laserSize {
		gs.directionY = -gs.directionY
	}

	gc.SetFillColor(color.RGBA{0xff, 0xff, 0xff, 0xff})
	gc.Clear()

	// move red laser
	gs.laserX += gs.directionX
	gs.laserY += gs.directionY

	// draws red 🔴 laser
	gc.SetFillColor(color.RGBA{0xff, 0x00, 0x00, 0xff})
	gc.SetStrokeColor(color.RGBA{0xff, 0x00, 0x00, 0xff})

	gc.BeginPath()
	draw2dkit.Circle(gc, gs.laserX, gs.laserY, gs.laserSize)
	gc.FillStroke()
	gc.Close()

	return true
}
